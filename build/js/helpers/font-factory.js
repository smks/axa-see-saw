"use strict";
function default_1(game, options, underline) {
    if (underline === void 0) { underline = false; }
    var text = game.add.text(options.xPos, options.yPos, options.content, {
        font: options.fontSize + ' ' + options.font,
        fill: options.color,
        align: "center"
    });
    text.anchor.set(0.5, 0.5);
    text.lineSpacing = options.lineSpacing;
    if (underline) {
        text.fontStyle = 'underline';
    }
    return text;
}
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = default_1;
