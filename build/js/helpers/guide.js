"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Guider = (function (_super) {
    __extends(Guider, _super);
    function Guider() {
        _super.apply(this, arguments);
    }
    Guider.prototype.create = function () {
    };
    Guider.prototype.update = function () {
    };
    Guider.prototype.destroy = function () {
    };
    return Guider;
}(Phaser.Group));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Guider;
