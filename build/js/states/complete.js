"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Complete = (function (_super) {
    __extends(Complete, _super);
    function Complete() {
        _super.apply(this, arguments);
    }
    Complete.prototype.create = function () {
        console.log('game complete');
    };
    return Complete;
}(Phaser.State));
exports.Complete = Complete;
