var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MyGame;
(function (MyGame) {
    var States;
    (function (States) {
        var Level2 = (function (_super) {
            __extends(Level2, _super);
            function Level2() {
                _super.call(this);
                console.log('level 2');
            }
            Level2.prototype.create = function () {
                _super.prototype.create.call(this);
            };
            Level2.prototype.update = function () {
                _super.prototype.update.call(this);
            };
            return Level2;
        }(Phaser.State));
        States.Level2 = Level2;
    })(States = MyGame.States || (MyGame.States = {}));
})(MyGame || (MyGame = {}));
