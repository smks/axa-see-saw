var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MyGame;
(function (MyGame) {
    var States;
    (function (States) {
        var Level1 = (function (_super) {
            __extends(Level1, _super);
            function Level1() {
                _super.call(this);
                console.log('level 1');
            }
            Level1.prototype.create = function () {
                _super.prototype.create.call(this);
            };
            Level1.prototype.update = function () {
                _super.prototype.update.call(this);
            };
            return Level1;
        }(Phaser.State));
        States.Level1 = Level1;
    })(States = MyGame.States || (MyGame.States = {}));
})(MyGame || (MyGame = {}));
