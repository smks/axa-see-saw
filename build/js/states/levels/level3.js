var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MyGame;
(function (MyGame) {
    var States;
    (function (States) {
        var Level3 = (function (_super) {
            __extends(Level3, _super);
            function Level3() {
                _super.call(this);
                console.log('level 3');
            }
            Level3.prototype.create = function () {
                _super.prototype.create.call(this);
            };
            Level3.prototype.update = function () {
                _super.prototype.update.call(this);
            };
            return Level3;
        }(Phaser.State));
        States.Level3 = Level3;
    })(States = MyGame.States || (MyGame.States = {}));
})(MyGame || (MyGame = {}));
