"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var seesaw_1 = require("../views/seesaw");
var seesaw_controller_1 = require("../controllers/seesaw.controller");
var seesaw_2 = require("../models/seesaw");
var banker_1 = require("../characters/banker");
var vertical_slider_1 = require("../models/vertical-slider");
var vertical_slider_2 = require("../views/vertical-slider");
var vertical_slider_controller_1 = require("../controllers/vertical-slider.controller");
var Point = Phaser.Point;
var horizontal_slider_1 = require("../models/horizontal-slider");
var horizontal_slider_2 = require("../views/horizontal-slider");
var horizontal_slider_controller_1 = require("../controllers/horizontal-slider.controller");
var Play = (function (_super) {
    __extends(Play, _super);
    function Play() {
        _super.call(this);
    }
    Play.prototype.create = function () {
        this.graphChangeUpEvent = new Phaser.Signal();
        this.graphChangeDownEvent = new Phaser.Signal();
        this.background = this.game.add.sprite(0, 0, 'sprites');
        this.background.frameName = 'background.jpg';
        this.banker = new banker_1.default(this.game);
        this.banker.x = 29;
        this.banker.y = 54;
        this.banker.create();
        this.seeSawModel = new seesaw_2.default();
        this.seeSawView = new seesaw_1.default(this.game);
        this.seeSawController = new seesaw_controller_1.default(this.seeSawModel, this.seeSawView);
        this.verticalSliderModel = new vertical_slider_1.default(254, 169, 31, 194);
        this.verticalSliderView = new vertical_slider_2.default(this.game);
        this.verticalSliderController = new vertical_slider_controller_1.default(this.verticalSliderModel, this.verticalSliderView);
        this.verticalSliderController.updateView();
        this.horizontalSliderModel = new horizontal_slider_1.default(442, 167, 442, 652);
        this.horizontalSliderView = new horizontal_slider_2.default(this.game);
        this.horizontalSliderController = new horizontal_slider_controller_1.default(this.horizontalSliderModel, this.horizontalSliderView);
        this.horizontalSliderController.updateView();
        this.trees = this.game.add.sprite(411, 306, 'sprites');
        this.trees.frameName = 'trees.png';
        this.startPoint = new Point(0, 0);
        this.endPoint = new Point(0, 0);
        this.minEndY = 135;
        this.maxEndY = 260;
        this.midEndY = (this.minEndY + this.minEndY / 2);
        this.setDefaultPosition();
    };
    Play.prototype.update = function () {
        this.startPoint.x = this.verticalSliderModel.xPos;
        this.startPoint.y = this.verticalSliderModel.yPos;
        this.endPoint.x = this.horizontalSliderModel.xPos;
        this.endPoint.y = this.horizontalSliderModel.yPos;
        this.banker.move(this.verticalSliderModel.getPosition());
        this.horizontalSliderController.updateView();
        if (this.horizontalSliderController.shouldPulse && this.verticalSliderController.shouldPulse) {
            this.horizontalSliderController.updatePulse();
            this.verticalSliderController.updatePulse();
        }
        this.seeSawController.updateModel(this.startPoint, this.endPoint, this.horizontalSliderModel.maxXPos);
        this.seeSawController.updateView();
    };
    Play.prototype.shutdown = function () {
        this.characterChangeEvent.dispose();
    };
    Play.prototype.setDefaultPosition = function () {
        this.horizontalSliderController.setPosition(566.8559670781893);
    };
    return Play;
}(Phaser.State));
exports.Play = Play;
