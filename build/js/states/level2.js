"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Level2 = (function (_super) {
    __extends(Level2, _super);
    function Level2() {
        _super.apply(this, arguments);
    }
    Level2.prototype.create = function () {
        this.levelText = this.game.add.text(this.game.world.centerX, this.game.world.centerY, 'Level 2', {
            font: "65px Open Sans",
            fill: "#ffffff",
            align: "center"
        });
        this.levelText.anchor.set(0.5, 0.5);
    };
    Level2.prototype.destroy = function () {
    };
    return Level2;
}(Phaser.State));
exports.Level2 = Level2;
