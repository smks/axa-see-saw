"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Preloader = (function (_super) {
    __extends(Preloader, _super);
    function Preloader() {
        _super.apply(this, arguments);
    }
    Preloader.prototype.preload = function () {
        this.game.stage.backgroundColor = "#ffffff";
        this.loading = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loading');
        this.loading.anchor.set(0.5, 0.5);
        this.game.load.atlasJSONArray('sprites', 'assets/atlas/axa-see-saw.png', null, "{\"frames\": [\n                {\n                    \"filename\": \"background.jpg\",\n                    \"frame\": {\"x\":2,\"y\":2,\"w\":740,\"h\":342},\n                    \"rotated\": false,\n                    \"trimmed\": false,\n                    \"spriteSourceSize\": {\"x\":0,\"y\":0,\"w\":740,\"h\":342},\n                    \"sourceSize\": {\"w\":740,\"h\":342}\n                },\n                {\n                    \"filename\": \"banker-arm.png\",\n                    \"frame\": {\"x\":69,\"y\":471,\"w\":78,\"h\":24},\n                    \"rotated\": false,\n                    \"trimmed\": false,\n                    \"spriteSourceSize\": {\"x\":0,\"y\":0,\"w\":78,\"h\":24},\n                    \"sourceSize\": {\"w\":78,\"h\":24}\n                },\n                {\n                    \"filename\": \"banker-head.png\",\n                    \"frame\": {\"x\":995,\"y\":2,\"w\":24,\"h\":30},\n                    \"rotated\": false,\n                    \"trimmed\": false,\n                    \"spriteSourceSize\": {\"x\":0,\"y\":0,\"w\":24,\"h\":30},\n                    \"sourceSize\": {\"w\":24,\"h\":30}\n                },\n                {\n                    \"filename\": \"banker.png\",\n                    \"frame\": {\"x\":2,\"y\":346,\"w\":65,\"h\":149},\n                    \"rotated\": false,\n                    \"trimmed\": false,\n                    \"spriteSourceSize\": {\"x\":0,\"y\":0,\"w\":65,\"h\":149},\n                    \"sourceSize\": {\"w\":65,\"h\":149}\n                },\n                {\n                    \"filename\": \"blue-sphere-light20.png\",\n                    \"frame\": {\"x\":223,\"y\":346,\"w\":46,\"h\":46},\n                    \"rotated\": false,\n                    \"trimmed\": false,\n                    \"spriteSourceSize\": {\"x\":0,\"y\":0,\"w\":46,\"h\":46},\n                    \"sourceSize\": {\"w\":46,\"h\":46}\n                },\n                {\n                    \"filename\": \"blue-sphere.png\",\n                    \"frame\": {\"x\":223,\"y\":394,\"w\":46,\"h\":46},\n                    \"rotated\": false,\n                    \"trimmed\": false,\n                    \"spriteSourceSize\": {\"x\":0,\"y\":0,\"w\":46,\"h\":46},\n                    \"sourceSize\": {\"w\":46,\"h\":46}\n                },\n                {\n                    \"filename\": \"pink-sphere-light20.png\",\n                    \"frame\": {\"x\":223,\"y\":442,\"w\":50,\"h\":43},\n                    \"rotated\": false,\n                    \"trimmed\": false,\n                    \"spriteSourceSize\": {\"x\":0,\"y\":0,\"w\":50,\"h\":43},\n                    \"sourceSize\": {\"w\":50,\"h\":43}\n                },\n                {\n                    \"filename\": \"pink-sphere.png\",\n                    \"frame\": {\"x\":271,\"y\":346,\"w\":50,\"h\":43},\n                    \"rotated\": false,\n                    \"trimmed\": false,\n                    \"spriteSourceSize\": {\"x\":0,\"y\":0,\"w\":50,\"h\":43},\n                    \"sourceSize\": {\"w\":50,\"h\":43}\n                },\n                {\n                    \"filename\": \"trees.png\",\n                    \"frame\": {\"x\":744,\"y\":2,\"w\":249,\"h\":36},\n                    \"rotated\": false,\n                    \"trimmed\": true,\n                    \"spriteSourceSize\": {\"x\":1,\"y\":0,\"w\":249,\"h\":36},\n                    \"sourceSize\": {\"w\":250,\"h\":36}\n                },\n                {\n                    \"filename\": \"year-duration.png\",\n                    \"frame\": {\"x\":69,\"y\":346,\"w\":152,\"h\":123},\n                    \"rotated\": false,\n                    \"trimmed\": false,\n                    \"spriteSourceSize\": {\"x\":0,\"y\":0,\"w\":152,\"h\":123},\n                    \"sourceSize\": {\"w\":152,\"h\":123}\n                }],\n                \"meta\": {\n                    \"app\": \"http://www.codeandweb.com/texturepacker\",\n                    \"version\": \"1.0\",\n                    \"image\": \"axa-see-saw.png\",\n                    \"format\": \"RGBA8888\",\n                    \"size\": {\"w\":1024,\"h\":512},\n                    \"scale\": \"1\",\n                    \"smartupdate\": \"$TexturePacker:SmartUpdate:2ba85951ffe3d9ccaf9059af3462883e:826e07fc41fc4e9054700b50972813c8:197cccb4b7690ee95b901ca79463e7f7$\"\n                }\n                }");
    };
    Preloader.prototype.update = function () {
        this.loading.rotation += 0.1;
        if (this.loading.rotation >= 360) {
            this.loading.rotation = 0;
        }
    };
    Preloader.prototype.create = function () {
        var _this = this;
        var tween = this.game.add.tween(this.loading)
            .to({ alpha: 0 }, 1000, "Linear", false);
        tween.onComplete.add(function () {
            _this.game.state.start('Play', true);
        });
        tween.start();
    };
    Preloader.prototype.shutdown = function () {
        this.loading = null;
    };
    return Preloader;
}(Phaser.State));
exports.Preloader = Preloader;
