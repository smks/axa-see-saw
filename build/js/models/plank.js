"use strict";
var SeeSawModel = (function () {
    function SeeSawModel(position) {
        this.positionMap = {
            pos_1: {
                xPos: 213,
                yPos: 172.5,
                angle: 0,
                width: 455 // Flat Line
            },
            pos_2: {
                xPos: 213,
                yPos: 142,
                angle: 8.5,
                width: 467.25
            },
            pos_3: {
                xPos: 208,
                yPos: 105.25,
                angle: 16,
                width: 486
            },
            pos_4: {
                xPos: 212,
                yPos: 72,
                angle: 24,
                width: 503.75
            },
            pos_5: {
                xPos: 211,
                yPos: 38,
                angle: 30,
                width: 526 // Highest Angle
            }
        };
        this.position = position;
        this.setCoordinates(this.position);
    }
    /**
     *
     * @param position
     */
    SeeSawModel.prototype.setCoordinates = function (position) {
        this.position = position;
        this.currentCoords = this.positionMap[("pos_" + this.position)];
        console.log('setting coords for graph');
        console.log(this.currentCoords);
    };
    Object.defineProperty(SeeSawModel.prototype, "xPos", {
        get: function () {
            return this.currentCoords.xPos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SeeSawModel.prototype, "yPos", {
        get: function () {
            return this.currentCoords.yPos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SeeSawModel.prototype, "angle", {
        get: function () {
            return this.currentCoords.angle;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SeeSawModel.prototype, "width", {
        get: function () {
            return this.currentCoords.width;
        },
        enumerable: true,
        configurable: true
    });
    return SeeSawModel;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SeeSawModel;
