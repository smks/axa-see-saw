"use strict";
var Plank = (function () {
    /**
     *
     * @param xPos
     * @param yPos
     * @param radius
     */
    function Plank(xPos, yPos, radius) {
        this._xPos = xPos;
        this._yPos = yPos;
        this._radius = radius;
    }
    Object.defineProperty(Plank.prototype, "xPos", {
        get: function () {
            return this._xPos;
        },
        set: function (xPos) {
            if (xPos < this.minXPos) {
                this._xPos = this.minXPos;
                return;
            }
            if (xPos > this.maxXPos) {
                this._xPos = this.maxXPos;
                return;
            }
            this._xPos = xPos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Plank.prototype, "yPos", {
        get: function () {
            return this._yPos;
        },
        set: function (yPos) {
            this._yPos = yPos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Plank.prototype, "radius", {
        get: function () {
            return this._radius;
        },
        set: function (radius) {
            if (radius < this.minRadius) {
                this._radius = this.minRadius;
                return;
            }
            if (radius > this.maxRadius) {
                this._radius = this.maxRadius;
                return;
            }
            this._radius = radius;
        },
        enumerable: true,
        configurable: true
    });
    return Plank;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Plank;
