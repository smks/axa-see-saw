"use strict";
var SeeSawModel = (function () {
    function SeeSawModel() {
    }
    /**
     *
     * @param position
     */
    SeeSawModel.prototype.update = function (startPoint, endPoint, farRightX) {
        this.start = startPoint;
        this.end = endPoint;
        this.farRightX = farRightX;
    };
    return SeeSawModel;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SeeSawModel;
