"use strict";
var Point = Phaser.Point;
var VerticalSliderModel = (function () {
    /**
     *
     * @param yPos
     * @param minYPos
     * @param maxYPos
     */
    function VerticalSliderModel(xPos, yPos, minYPos, maxYPos) {
        this.currentCoords = new Point(xPos, yPos);
        this.minYPos = minYPos;
        this.maxYPos = maxYPos;
        this.movePosition(yPos);
    }
    /**
     *
     * @param position
     */
    VerticalSliderModel.prototype.movePosition = function (yPosition) {
        if (yPosition < this.minYPos) {
            yPosition = this.minYPos;
        }
        else if (yPosition > this.maxYPos) {
            yPosition = this.maxYPos;
        }
        this.currentCoords.y = yPosition;
    };
    Object.defineProperty(VerticalSliderModel.prototype, "xPos", {
        get: function () {
            return this.currentCoords.x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(VerticalSliderModel.prototype, "yPos", {
        get: function () {
            return this.currentCoords.y;
        },
        enumerable: true,
        configurable: true
    });
    VerticalSliderModel.prototype.getPosition = function () {
        return this.currentCoords;
    };
    return VerticalSliderModel;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = VerticalSliderModel;
