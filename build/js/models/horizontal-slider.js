"use strict";
var Point = Phaser.Point;
var HorizontalSliderModel = (function () {
    /**
     *
     * @param yPos
     * @param minYPos
     * @param maxYPos
     */
    function HorizontalSliderModel(xPos, yPos, minXPos, maxXPos) {
        this.currentNumberOfYears = 25;
        this.maxNumberOfYears = 25;
        this.isPulsing = true;
        this.currentCoords = new Point(xPos, yPos);
        this.minXPos = minXPos;
        this.maxXPos = maxXPos;
        this.movePosition(xPos);
        var incrementStep = (this.maxXPos - this.minXPos) / this.maxNumberOfYears;
        this.increments = {};
        var currentIncrement = this.minXPos;
        for (var i = this.maxNumberOfYears; i >= 0; i--) {
            if (i === 0) {
                this.increments[i] = this.maxXPos;
                continue;
            }
            if (i === this.maxNumberOfYears) {
                this.increments[i] = this.minXPos;
                continue;
            }
            currentIncrement += incrementStep;
            this.increments[i] = currentIncrement;
        }
    }
    /**
     *
     * @param position
     */
    HorizontalSliderModel.prototype.movePosition = function (xPosition) {
        if (xPosition < this.minXPos) {
            xPosition = this.minXPos;
        }
        else if (xPosition > this.maxXPos) {
            xPosition = this.maxXPos;
        }
        for (var key in this.increments) {
            if (this.increments.hasOwnProperty(key)) {
                if (xPosition <= this.increments[key]) {
                    this.currentNumberOfYears = parseInt(key);
                }
            }
        }
        this.currentCoords.x = xPosition;
    };
    Object.defineProperty(HorizontalSliderModel.prototype, "xPos", {
        get: function () {
            return this.currentCoords.x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HorizontalSliderModel.prototype, "yPos", {
        get: function () {
            return this.currentCoords.y;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HorizontalSliderModel.prototype, "years", {
        get: function () {
            return this.currentNumberOfYears;
        },
        enumerable: true,
        configurable: true
    });
    HorizontalSliderModel.prototype.getPosition = function () {
        return this.currentCoords;
    };
    HorizontalSliderModel.prototype.getMinX = function () {
        return this.minXPos;
    };
    HorizontalSliderModel.prototype.setPulse = function (isPulsing) {
        this.isPulsing = isPulsing;
    };
    return HorizontalSliderModel;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = HorizontalSliderModel;
