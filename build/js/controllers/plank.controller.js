"use strict";
var PlankController = (function () {
    function PlankController(model, view) {
        this.plankModel = model;
        this.plankView = view;
        this.plankView.create();
    }
    /**
     *
     * @param position
     */
    PlankController.prototype.setPosition = function (position) {
        this.plankModel.setCoordinates(position);
    };
    PlankController.prototype.updateView = function (isTween) {
        if (isTween === void 0) { isTween = true; }
        this.plankView.update(this.plankModel.xPos, this.plankModel.yPos, this.plankModel.angle, this.plankModel.width, isTween);
    };
    return PlankController;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PlankController;
