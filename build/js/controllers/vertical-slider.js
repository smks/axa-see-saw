"use strict";
var VerticalSliderController = (function () {
    function VerticalSliderController(model, view) {
        this.verticalSliderModel = model;
        this.verticalSliderView = view;
        this.verticalSliderView.create();
    }
    /**
     *
     * @param position
     */
    VerticalSliderController.prototype.setPosition = function (position) {
        this.verticalSliderModel.movePosition(position);
    };
    VerticalSliderController.prototype.updateView = function (isTween) {
        if (isTween === void 0) { isTween = true; }
        this.verticalSliderView.update(this.verticalSliderModel.xPos, this.verticalSliderModel.yPos, this.verticalSliderModel.angle, this.verticalSliderModel.width, isTween);
    };
    return VerticalSliderController;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = VerticalSliderController;
