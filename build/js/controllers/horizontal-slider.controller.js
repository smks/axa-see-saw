"use strict";
var HorizontalSliderController = (function () {
    function HorizontalSliderController(model, view) {
        var _this = this;
        this.previousSeconds = 0;
        this.shouldPulse = true;
        this.horizontalSliderModel = model;
        this.horizontalSliderView = view;
        this.horizontalSliderView.create();
        this.horizontalSliderView.bindDragEvents(function () {
            _this.shouldPulse = false;
        }, function (sprite, pointer, dragX, dragY, snapPoint) {
            sprite.y = _this.horizontalSliderModel.yPos;
            _this.setPosition(dragX);
        }, function () {
            _this.shouldPulse = true;
        });
    }
    HorizontalSliderController.prototype.setPosition = function (xPos) {
        this.horizontalSliderModel.movePosition(xPos);
    };
    HorizontalSliderController.prototype.updateView = function () {
        this.horizontalSliderView.update(this.horizontalSliderModel.xPos, this.horizontalSliderModel.yPos, this.horizontalSliderModel.years);
    };
    HorizontalSliderController.prototype.updatePulse = function () {
        if (!this.shouldPulse) {
            return;
        }
        var d = new Date();
        var seconds = d.getSeconds();
        if (seconds % 3 === 0 && this.previousSeconds != seconds) {
            this.previousSeconds = seconds;
            this.pulse();
        }
    };
    HorizontalSliderController.prototype.pulse = function () {
        this.horizontalSliderView.pulse(this.shouldPulse);
    };
    HorizontalSliderController.prototype.getPosition = function () {
        return this.horizontalSliderModel.getPosition();
    };
    HorizontalSliderController.prototype.getY = function () {
        return this.horizontalSliderView.yPosition();
    };
    return HorizontalSliderController;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = HorizontalSliderController;
