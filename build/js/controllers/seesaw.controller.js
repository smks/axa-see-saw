"use strict";
var SeeSawController = (function () {
    function SeeSawController(model, view) {
        this.seesawModel = model;
        this.seesawView = view;
        this.seesawView.create();
    }
    /**
     *
     * @param position
     */
    SeeSawController.prototype.updateModel = function (startPoint, midPoint, farRightX) {
        this.seesawModel.update(startPoint, midPoint, farRightX);
    };
    SeeSawController.prototype.updateView = function () {
        this.seesawView.update(this.seesawModel.start, this.seesawModel.end, this.seesawModel.farRightX);
    };
    return SeeSawController;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SeeSawController;
