"use strict";
var BallController = (function () {
    function BallController(ballModel, ballView) {
        this.ballModel = ballModel;
        this.ballView = ballView;
    }
    BallController.prototype.setXPos = function (xPos) {
        this.ballModel.xPos = xPos;
    };
    BallController.prototype.setYPos = function (yPos) {
        this.ballModel.yPos = yPos;
    };
    BallController.prototype.setRadius = function (radius) {
        this.ballModel.radius = radius;
    };
    BallController.prototype.updateView = function () {
        this.ballView.update(this.ballModel.xPos, this.ballModel.yPos, this.ballModel.angle);
    };
    return BallController;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BallController;
