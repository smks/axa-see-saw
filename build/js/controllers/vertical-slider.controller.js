"use strict";
var VerticalSliderController = (function () {
    function VerticalSliderController(model, view) {
        var _this = this;
        this.shouldPulse = true;
        this.previousSeconds = 0;
        this.verticalSliderModel = model;
        this.verticalSliderView = view;
        this.verticalSliderView.create();
        this.verticalSliderView.bindDragEvents(function () {
            _this.shouldPulse = false;
        }, function (sprite, pointer, dragX, dragY, snapPoint) {
            sprite.x = _this.verticalSliderModel.xPos;
            _this.setPosition(dragY);
        }, function () {
            _this.shouldPulse = true;
        });
    }
    VerticalSliderController.prototype.setPosition = function (yPos) {
        this.verticalSliderModel.movePosition(yPos);
    };
    VerticalSliderController.prototype.updatePulse = function () {
        if (!this.shouldPulse) {
            return;
        }
        var d = new Date();
        var seconds = d.getSeconds();
        if (seconds % 3 === 0 && this.previousSeconds != seconds) {
            this.previousSeconds = seconds;
            this.pulse();
        }
    };
    VerticalSliderController.prototype.updateView = function () {
        this.verticalSliderView.update(this.verticalSliderModel.xPos, this.verticalSliderModel.yPos);
    };
    VerticalSliderController.prototype.pulse = function () {
        this.verticalSliderView.pulse(this.shouldPulse);
    };
    return VerticalSliderController;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = VerticalSliderController;
