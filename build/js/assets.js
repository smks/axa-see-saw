"use strict";
var Assets = (function () {
    function Assets() {
    }
    Assets.LOADING_ICON = 'assets/preload/loading.png';
    Assets.ARROW_FALL = 'arrow-fall.png';
    Assets.ARROW_RISE = 'arrow-rise.png';
    Assets.BACKGROUND = 'background.jpg';
    Assets.CHARACTER_1_ARM = 'character-1-arm.png';
    Assets.CHARACTER_1 = 'character-1.png';
    Assets.CHARACTER_2_ARM = 'character-2-arm.png';
    Assets.CHARACTER_2 = 'character-2.png';
    Assets.CHARACTER_3_ARM = 'character-3-arm.png';
    Assets.CHARACTER_3 = 'character-3.png';
    Assets.CHOOSE_CHARACTER_1 = 'choose-character-1.png';
    Assets.CHOOSE_CHARACTER_2 = 'choose-character-2.png';
    Assets.CHOOSE_CHARACTER_3 = 'choose-character-3.png';
    Assets.COIN_ARROW_LEFT = 'coin-arrow-left.png';
    Assets.COIN_ARROW_RIGHT = 'coin-arrow-right.png';
    Assets.COIN_DURATION_20_YEARS = 'coin-duration-20-years.png';
    Assets.COIN_DURATION_5_YEARS = 'coin-duration-5-years.png';
    Assets.COIN = 'coin.png';
    return Assets;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Assets;
