"use strict";
var DutchTranslation = (function () {
    function DutchTranslation() {
        /**
         *
         * @type {{a: string, b: string, c: string}}
         */
        this.translationMap = {
            'a': 'Dit is een',
            'b': 'Dit is B',
            'c': 'Dit C'
        };
    }
    /**
     *
     * @param key
     * @returns {string}
     */
    DutchTranslation.prototype.translate = function (key) {
        if (this.translationMap[key] !== null) {
            return this.translationMap[key];
        }
        return key;
    };
    return DutchTranslation;
}());
exports.DutchTranslation = DutchTranslation;
