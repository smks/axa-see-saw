"use strict";
var EnglishTranslation = (function () {
    function EnglishTranslation() {
        this.translationMap = {
            'a': 'This is A',
            'b': 'This is B',
            'c': 'This is C'
        };
    }
    EnglishTranslation.prototype.translate = function (key) {
        if (this.translationMap[key] !== null) {
            return this.translationMap[key];
        }
        return key;
    };
    return EnglishTranslation;
}());
exports.EnglishTranslation = EnglishTranslation;
