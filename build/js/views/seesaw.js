"use strict";
var Point = Phaser.Point;
var SeeSawView = (function () {
    /**
     *
     * @param game
     */
    function SeeSawView(game) {
        this.game = game;
    }
    SeeSawView.prototype.create = function () {
        this.bmd = this.game.add.bitmapData(this.game.width, this.game.height);
        this.bmd.ctx.beginPath();
        this.bmd.ctx.lineWidth = 5;
        this.bmd.ctx.strokeStyle = 'rgb(226, 0, 122)';
        this.bmd.ctx.stroke();
        this.sprite = this.game.add.sprite(0, 0, this.bmd);
    };
    /**
     *
     * @param xPos
     * @param yPos
     * @param angle
     */
    SeeSawView.prototype.update = function (start, mid, farRightX) {
        if (farRightX === void 0) { farRightX = 0; }
        var point1 = new Point(start.x, start.y); // P1
        var point2 = new Point(mid.x, mid.y); // P2
        var point3 = new Point(farRightX, point1.y); // P3
        var angle = Math.atan2(point2.y - point1.y, point2.x - point1.x);
        var height = Math.tan(angle) * (point3.getMagnitude() - point1.getMagnitude());
        var endPoint = new Point(point3.x, point3.y + height);
        this.bmd.clear();
        this.bmd.ctx.beginPath();
        this.bmd.ctx.moveTo(start.x, start.y);
        this.bmd.ctx.lineTo(endPoint.x, endPoint.y);
        this.bmd.ctx.lineWidth = 4;
        this.bmd.ctx.stroke();
        this.bmd.ctx.closePath();
        this.bmd.render();
    };
    return SeeSawView;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = SeeSawView;
