"use strict";
var BallView = (function () {
    /**
     *
     * @param game
     */
    function BallView(game) {
        this.game = game;
    }
    BallView.prototype.create = function () {
        this.background = this.game.add.graphics(0, 0);
        this.background.lineStyle(1, 0xcccccc, 1);
        this.background.beginFill(0xcccccc, 1);
        this.background.drawCircle(0, 0, 36);
        this.background.endFill();
    };
    /**
     *
     * @param xPos
     * @param yPos
     * @param radius
     */
    BallView.prototype.update = function (xPos, yPos, radius) {
    };
    return BallView;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = BallView;
