"use strict";
var Quadratic = Phaser.Easing.Quadratic;
var VerticalSliderView = (function () {
    /**
     *
     * @param game
     */
    function VerticalSliderView(game) {
        this.game = game;
    }
    VerticalSliderView.prototype.create = function () {
        this.circle = this.game.add.sprite(0, 0, 'sprites', 'pink-sphere.png');
        this.circle.anchor.set(0.5, 0.5);
        this.circle.inputEnabled = true;
        this.circle.input.enableDrag(false, false, true, 1, new Phaser.Rectangle(226, 10, 0, 209));
    };
    /**
     *
     * @param dragStart
     * @param dragUpdate
     * @param dragStop
     */
    VerticalSliderView.prototype.bindDragEvents = function (dragStart, dragUpdate, dragStop) {
        this.circle.events.onDragStart.add(dragStart);
        this.circle.events.onDragUpdate.add(dragUpdate);
        this.circle.events.onDragStop.add(dragStop);
    };
    /**
     *
     * @param xPos
     * @param yPos
     * @param angle
     */
    VerticalSliderView.prototype.update = function (xPos, yPos) {
        this.circle.x = xPos;
        this.circle.y = yPos;
    };
    VerticalSliderView.prototype.pulse = function (shouldPulse) {
        var _this = this;
        if (shouldPulse === void 0) { shouldPulse = true; }
        if (!shouldPulse) {
            return;
        }
        this.circle.frameName = 'pink-sphere-light20.png';
        var tween = this.game.add
            .tween(this.circle.scale)
            .to({ x: 1.1, y: 1.1 }, 300, Quadratic.InOut, false, 1, 0, true);
        tween.onComplete.add(function () {
            _this.circle.frameName = 'pink-sphere.png';
        });
        tween.start();
    };
    return VerticalSliderView;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = VerticalSliderView;
