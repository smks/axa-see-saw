"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var character_1 = require("../characters/character");
var Coin = (function (_super) {
    __extends(Coin, _super);
    /**
     *
     * @param game
     */
    function Coin(game, character) {
        if (character === void 0) { character = character_1.CHARACTERS.JANET; }
        _super.call(this, game);
        this.character = character;
    }
    Coin.prototype.create = function () {
        this.background = this.game.add.sprite(0, 0, 'sprites');
        this.background.frameName = 'coin.png';
        this.add(this.background);
        this.coinDurationTwentyYears = this.game.add.sprite(0, 0, 'sprites');
        this.coinDurationTwentyYears.frameName = 'coin-duration-20-years.png';
        this.coinDurationTwentyYears.anchor.set(0.5, 0.5);
        this.coinDurationTwentyYears.x = this.background.width / 2;
        this.coinDurationTwentyYears.y = this.background.height / 2;
        this.add(this.coinDurationTwentyYears);
        this.coinDurationFiveYears = this.game.add.sprite(0, 0, 'sprites');
        this.coinDurationFiveYears.frameName = 'coin-duration-5-years.png';
        this.coinDurationFiveYears.anchor.set(0.5, 0.5);
        this.coinDurationFiveYears.x = this.background.width / 2;
        this.coinDurationFiveYears.y = this.background.height / 2;
        this.add(this.coinDurationFiveYears);
        this.determineCoinDuration();
    };
    /**
     *
     * @param character
     */
    Coin.prototype.setCharacter = function (character) {
        this.character = character;
        this.determineCoinDuration();
    };
    Coin.prototype.determineCoinDuration = function () {
        switch (this.character) {
            case character_1.CHARACTERS.JANET:
                this.coinDurationFiveYears.visible = true;
                this.coinDurationTwentyYears.visible = false;
                break;
            case character_1.CHARACTERS.MARK:
                this.coinDurationFiveYears.visible = false;
                this.coinDurationTwentyYears.visible = true;
                break;
            case character_1.CHARACTERS.MARIO:
                this.coinDurationFiveYears.visible = false;
                this.coinDurationTwentyYears.visible = true;
                break;
        }
    };
    Coin.prototype.destroy = function () {
    };
    return Coin;
}(Phaser.Group));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Coin;
