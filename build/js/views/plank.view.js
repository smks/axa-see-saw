"use strict";
var PlankView = (function () {
    /**
     *
     * @param game
     */
    function PlankView(game) {
        this.game = game;
    }
    PlankView.prototype.create = function () {
        this.background = this.game.add.graphics(0, 0);
        this.background.lineStyle(1, 0xcccccc, 1);
        this.background.beginFill(0xcccccc, 1);
        this.background.drawCircle(0, 0, 36);
        this.background.endFill();
    };
    /**
     *
     * @param xPos
     * @param yPos
     * @param radius
     */
    PlankView.prototype.update = function (xPos, yPos, radius) {
    };
    return PlankView;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PlankView;
