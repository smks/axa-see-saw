"use strict";
var Group = Phaser.Group;
var font_factory_1 = require("./../helpers/font-factory");
var Quadratic = Phaser.Easing.Quadratic;
var HorizontalSliderView = (function () {
    /**
     *
     * @param game
     */
    function HorizontalSliderView(game) {
        this.years = 25;
        this.game = game;
    }
    HorizontalSliderView.prototype.create = function () {
        this.circleGroup = new Group(this.game);
        this.circle = this.game.add.sprite(0, 0, 'sprites', 'blue-sphere.png');
        this.circle.anchor.set(0.5, 0);
        this.circle.inputEnabled = true;
        this.circle.input.enableDrag(false, false, true, 1, new Phaser.Rectangle(419, 190, 257, 0));
        this.title = font_factory_1.default(this.game, {
            xPos: this.circle.x,
            yPos: this.circle.y,
            content: this.years.toString(),
            fontSize: '30px',
            lineSpacing: 1,
            font: 'Myriad Pro',
            color: '#ffffff'
        });
        this.circleGroup.add(this.circle);
        this.circleGroup.add(this.title);
        this.label = this.game.add.sprite(0, 0, 'sprites', 'year-duration.png');
        this.label.anchor.set(0.5, 0);
    };
    /**
     *
     * @param dragStart
     * @param dragUpdate
     * @param dragStop
     */
    HorizontalSliderView.prototype.bindDragEvents = function (dragStart, dragUpdate, dragStop) {
        this.circle.events.onDragStart.add(dragStart);
        this.circle.events.onDragUpdate.add(dragUpdate);
        this.circle.events.onDragStop.add(dragStop);
    };
    /**
     *
     * @param xPos
     * @param yPos
     * @param angle
     */
    HorizontalSliderView.prototype.update = function (xPos, yPos, years) {
        this.circle.x = xPos;
        this.circle.y = yPos;
        this.title.x = this.circle.x;
        this.title.y = this.circle.centerY + 4;
        this.years = years;
        this.title.text = this.years.toString();
        this.label.x = this.circle.x - (this.label.width / 2) + 3;
        this.label.y = this.circle.y * 1.31;
    };
    HorizontalSliderView.prototype.xPosition = function () {
        return this.circle.x;
    };
    HorizontalSliderView.prototype.yPosition = function () {
        return this.circle.y;
    };
    HorizontalSliderView.prototype.pulse = function (shouldPulse) {
        var _this = this;
        if (shouldPulse === void 0) { shouldPulse = true; }
        if (!shouldPulse) {
            return;
        }
        this.circle.frameName = 'blue-sphere-light20.png';
        var tween = this.game.add
            .tween(this.circle.scale)
            .to({ x: 1.1, y: 1.1 }, 300, Quadratic.InOut, false, 1, 0, true);
        tween.onComplete.add(function () {
            _this.circle.frameName = 'blue-sphere.png';
        });
        tween.start();
    };
    return HorizontalSliderView;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = HorizontalSliderView;
