"use strict";
var PlankView = (function () {
    /**
     *
     * @param game
     */
    function PlankView(game) {
        this.game = game;
    }
    PlankView.prototype.create = function () {
        this.plank = this.game.add.graphics(0, 0);
        this.plank.lineStyle(5, 0x9c9d9f, 1);
        this.plank.lineTo(450, 0);
        this.plank.endFill();
    };
    /**
     *
     * @param xPos
     * @param yPos
     * @param angle
     */
    PlankView.prototype.update = function (xPos, yPos, angle, width, isTween) {
        if (isTween === void 0) { isTween = true; }
        if (this.armTween && this.armTween.isRunning) {
            this.armTween.stop();
        }
        if (isTween) {
            this.armTween = this.game.add.tween(this.plank)
                .to({
                x: xPos,
                y: yPos,
                angle: angle,
                width: width
            }, 300, Phaser.Easing.Cubic.InOut, true);
            return;
        }
        this.plank.x = xPos;
        this.plank.y = yPos;
        this.plank.angle = angle;
        this.plank.width = width;
    };
    return PlankView;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = PlankView;
