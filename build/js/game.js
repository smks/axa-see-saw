/// <reference path='../../node_modules/phaser/typescript/phaser' />
"use strict";
var Game = Phaser.Game;
var boot_1 = require("./states/boot");
var preload_1 = require("./states/preload");
var play_1 = require("./states/play");
var MyGame = (function () {
    function MyGame() {
        this.game = new Game(740, 342, Phaser.AUTO, 'game', {
            create: this.create
        });
    }
    MyGame.prototype.create = function () {
        this.game.state.add('Boot', boot_1.Boot, false);
        this.game.state.add('Preloader', preload_1.Preloader, false);
        this.game.state.add('Play', play_1.Play, false);
        this.game.state.start('Boot');
    };
    MyGame.prototype.destroy = function () {
    };
    return MyGame;
}());
window.onload = function () {
    window['seesaw'] = new MyGame();
};
