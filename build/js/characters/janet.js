"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var add_sprites_1 = require("../characters/helpers/add-sprites");
var coords_1 = require("./helpers/coords");
var Banker = (function (_super) {
    __extends(Banker, _super);
    /**
     *
     * @param character
     * @param game
     */
    function Banker(character, game) {
        _super.call(this, game);
        this.characterId = '1';
        this.armPosition = 1;
        this.positionMap = {
            pos_1: {
                xPos: 71,
                yPos: 86,
                angle: 35 // Lowest arm
            },
            pos_2: {
                xPos: 57,
                yPos: 82,
                angle: 20
            },
            pos_3: {
                xPos: 50,
                yPos: 76.5,
                angle: 0
            },
            pos_4: {
                xPos: 61,
                yPos: 70,
                angle: -25
            },
            pos_5: {
                xPos: 78,
                yPos: 66,
                angle: -50 // Highest arm
            }
        };
        this.characterId = character;
        this.game = game;
    }
    Banker.prototype.create = function () {
        var coords = new coords_1.default(this.positionMap[("pos_" + this.armPosition)]);
        this.characterSprite = add_sprites_1.getCharacter(this.characterId, this.game);
        this.characterHead = getCharacterHead(this.characterId, this.game, coords);
        this.characterArm = add_sprites_1.getCharacterArm(this.characterId, this.game, coords);
        this.add(this.characterArm);
        this.add(this.characterSprite);
    };
    /**
     *
     * @param position
     */
    Banker.prototype.move = function (position) {
        this.armPosition = position;
        this.tween();
    };
    Banker.prototype.tween = function () {
        var coords = new coords_1.default(this.positionMap[("pos_" + this.armPosition)]);
        var newX = coords.xPos;
        var newY = coords.yPos;
        var newAngle = coords.angle;
        console.log(coords);
        if (this.armTween && this.armTween.isRunning) {
            this.armTween.stop();
        }
        this.armTween = this.game.add.tween(this.characterArm)
            .to({
            x: newX,
            y: newY,
            angle: newAngle
        }, 300, Phaser.Easing.Cubic.InOut, true);
    };
    return Banker;
}(Phaser.Group));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Banker;
