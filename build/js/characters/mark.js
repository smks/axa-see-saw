"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var banker_1 = require("./banker");
var Mark = (function (_super) {
    __extends(Mark, _super);
    /**
     *
     * @param character
     * @param game
     */
    function Mark(character, game) {
        _super.call(this, character, game);
        this.characterId = '3';
        this.positionMap = {
            pos_1: {
                xPos: 0,
                yPos: 0,
                angle: 0
            },
            pos_2: {
                xPos: 0,
                yPos: 0,
                angle: 0
            },
            pos_3: {
                xPos: 72,
                yPos: 87.5,
                angle: 0
            },
            pos_4: {
                xPos: 0,
                yPos: 0,
                angle: 0
            },
            pos_5: {
                xPos: 0,
                yPos: 0,
                angle: 0
            }
        };
    }
    return Mark;
}(banker_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Mark;
