"use strict";
var Coords = (function () {
    /**
     *
     * @param parameters
     */
    function Coords(parameters) {
        this._angle = 0;
        this._width = 0;
        this._xPos = parameters.xPos;
        this._yPos = parameters.yPos;
        this._angle = parameters.angle;
        this._width = parameters.width;
    }
    Object.defineProperty(Coords.prototype, "xPos", {
        get: function () {
            return this._xPos;
        },
        set: function (xpos) {
            this._xPos = xpos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Coords.prototype, "yPos", {
        get: function () {
            return this._yPos;
        },
        set: function (ypos) {
            this._yPos = ypos;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Coords.prototype, "angle", {
        get: function () {
            return this._angle;
        },
        set: function (angle) {
            this._angle = angle;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Coords.prototype, "width", {
        set: function (width) {
            this._width = width;
        },
        enumerable: true,
        configurable: true
    });
    return Coords;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Coords;
