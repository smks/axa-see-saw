"use strict";
/**
 *
 * @param characterId
 * @param game
 * @returns {Phaser.Group}
 */
function getCharacter(game) {
    var characterSprite = game.add.sprite(0, 0, 'sprites');
    characterSprite.frameName = "banker.png";
    return characterSprite;
}
exports.getCharacter = getCharacter;
/**
 *
 * @param characterId
 * @param game
 * @returns {Phaser.Group}
 */
function getCharacterArm(game) {
    var characterArm = game.add.sprite(0, 0, 'sprites');
    characterArm.x = 36;
    characterArm.y = 24;
    characterArm.angle = -45;
    characterArm.anchor.set(0, 0.5);
    characterArm.frameName = "banker-arm.png";
    return characterArm;
}
exports.getCharacterArm = getCharacterArm;
/**
 *
 * @param characterId
 * @param game
 * @returns {Phaser.Group}
 */
function getCharacterHead(game) {
    var characterHead = game.add.sprite(0, 0, 'sprites');
    characterHead.x = 49;
    characterHead.y = 0;
    characterHead.anchor.set(0.5, 0.5);
    characterHead.frameName = "banker-head.png";
    return characterHead;
}
exports.getCharacterHead = getCharacterHead;
