"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var add_sprites_1 = require("../characters/helpers/add-sprites");
var Banker = (function (_super) {
    __extends(Banker, _super);
    /**
     *
     * @param character
     * @param game
     */
    function Banker(game) {
        _super.call(this, game);
        this.game = game;
    }
    Banker.prototype.create = function () {
        this.characterSprite = add_sprites_1.getCharacter(this.game);
        this.characterHead = add_sprites_1.getCharacterHead(this.game);
        this.characterArm = add_sprites_1.getCharacterArm(this.game);
        this.add(this.characterArm);
        this.add(this.characterSprite);
        this.add(this.characterHead);
    };
    /**
     *
     * @param position
     */
    Banker.prototype.move = function (verticalSliderPosition) {
        var angleBetweenHead = Phaser.Math.angleBetween(this.characterHead.x, this.characterHead.y, verticalSliderPosition.x, verticalSliderPosition.y);
        var angleBetweenArm = Phaser.Math.angleBetween(this.characterArm.x, this.characterArm.y, verticalSliderPosition.x, verticalSliderPosition.y);
        this.characterArm.angle = angleBetweenArm * 90;
        this.characterHead.angle = angleBetweenHead * 50;
    };
    return Banker;
}(Phaser.Group));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Banker;
