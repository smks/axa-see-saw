var gulp = require('gulp'),
    ts = require('gulp-typescript'),
    tap = require('gulp-tap');
    sass = require('gulp-sass'),
    path = require('path'),
    htmlmin = require('gulp-html-minifier'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    flatten = require('gulp-flatten'),
    browserify = require('browserify'),
    sourcemaps = require('gulp-sourcemaps'),
    buffer = require('gulp-buffer');

gulp.task('default', [
    'minify-html',
    'copy-libs',
    'copy-assets',
    'scripts'
]);

gulp.task('watch', ['scripts'], function() {
    gulp.watch('src/typescript/**/*.ts', ['scripts']);
});

gulp.task('minify-html', function () {
    return gulp.src('./src/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('./dist'))
});

gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('copy-libs', function () {
    return gulp.src(['node_modules/phaser/build/phaser.min.js'])
        .pipe(flatten())
        .pipe(gulp.dest('dist/js'));
});

gulp.task('copy-assets', function () {
    return gulp.src('src/assets/**/*')
        .pipe(gulp.dest('dist/assets'));
});

gulp.task('scripts', function () {
    var tsProject = ts.createProject('tsconfig.json'),
        tsResult = gulp.src('./src/typescript/**/*.ts')
            .pipe(ts(tsProject));

    return tsResult
        .pipe(concat('game.js'))

        .pipe(tap(function(file) {
            file.contents = browserify(file.path, {debug: true}).bundle();
        }))

        .pipe(buffer())

        // load and init sourcemaps
        .pipe(sourcemaps.init({loadMaps: true}))

        .pipe(uglify())

        .pipe(sourcemaps.write('./'))

        .pipe(gulp.dest('dist/js'))
});