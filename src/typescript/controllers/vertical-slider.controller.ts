import VerticalSliderModel from "../models/vertical-slider";
import VerticalSliderView from "../views/vertical-slider";

export default class VerticalSliderController {

    verticalSliderModel:VerticalSliderModel;
    verticalSliderView:VerticalSliderView;
    shouldPulse:boolean = true;
    private previousSeconds:number = 0;

    constructor(model:VerticalSliderModel, view:VerticalSliderView) {
        this.verticalSliderModel = model;
        this.verticalSliderView = view;
        this.verticalSliderView.create();
        this.verticalSliderView.bindDragEvents(
            () => { // drag start
                this.shouldPulse = false;
            },
            (sprite, pointer, dragX, dragY, snapPoint) => {
                sprite.x = this.verticalSliderModel.xPos;
                this.setPosition(dragY);
            },
            () => { // drag end
                this.shouldPulse = true;
            }
        );
    }

    setPosition(yPos:number) {
        this.verticalSliderModel.movePosition(yPos);
    }

    updatePulse() {
        if (!this.shouldPulse) {
            return;
        }

        var d = new Date();
        var seconds = d.getSeconds();

        if (seconds % 3 === 0 && this.previousSeconds != seconds) {

            this.previousSeconds = seconds;

            this.pulse();
        }
    }

    updateView() {
        this.verticalSliderView.update(
            this.verticalSliderModel.xPos,
            this.verticalSliderModel.yPos
        );
    }

    pulse() {
        this.verticalSliderView.pulse(
            this.shouldPulse
        );
    }
}