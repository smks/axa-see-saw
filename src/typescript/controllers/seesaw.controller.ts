import SeeSawModel from "../models/seesaw";
import SeeSawView from "../views/seesaw.ts";
import Point = Phaser.Point;

export default class SeeSawController {

    seesawModel:SeeSawModel;
    seesawView:SeeSawView;

    constructor(model:SeeSawModel, view:SeeSawView) {
        this.seesawModel = model;
        this.seesawView = view;
        this.seesawView.create();
    }

    /**
     *
     * @param position
     */
    updateModel(startPoint:Point, midPoint:Point, farRightX:number) {
        this.seesawModel.update(startPoint, midPoint, farRightX);
    }

    updateView() {
        this.seesawView.update(
            this.seesawModel.start,
            this.seesawModel.end,
            this.seesawModel.farRightX
        )
    }
}