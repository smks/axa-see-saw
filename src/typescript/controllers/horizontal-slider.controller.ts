import HorizontalSliderModel from "../models/horizontal-slider";
import HorizontalSliderView from "../views/horizontal-slider";

export default class HorizontalSliderController {

    horizontalSliderModel:HorizontalSliderModel;
    horizontalSliderView:HorizontalSliderView;
    private previousSeconds:number = 0;
    shouldPulse:boolean = true;

    constructor(model:HorizontalSliderModel, view:HorizontalSliderView) {
        this.horizontalSliderModel = model;
        this.horizontalSliderView = view;
        this.horizontalSliderView.create();
        this.horizontalSliderView.bindDragEvents(
            () => { // drag start
                this.shouldPulse = false;
            },
            (sprite, pointer, dragX, dragY, snapPoint) => { // drag update
                sprite.y = this.horizontalSliderModel.yPos;
                this.setPosition(dragX);
            },
            () => { // drag end
                this.shouldPulse = true;
            }
        );
    }

    setPosition(xPos:number) {
        this.horizontalSliderModel.movePosition(xPos);
    }

    updateView() {
        this.horizontalSliderView.update(
            this.horizontalSliderModel.xPos,
            this.horizontalSliderModel.yPos,
            this.horizontalSliderModel.years
        );
    }

    updatePulse() {
        if (!this.shouldPulse) {
            return;
        }

        var d = new Date();
        var seconds = d.getSeconds();

        if (seconds % 3 === 0 && this.previousSeconds != seconds) {

            this.previousSeconds = seconds;

            this.pulse();
        }
    }

    pulse() {
        this.horizontalSliderView.pulse(
            this.shouldPulse
        );
    }

    getPosition():Phaser.Point {
        return this.horizontalSliderModel.getPosition();
    }

    getY() {
        return this.horizontalSliderView.yPosition();
    }
}