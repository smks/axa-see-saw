export default class Assets {

    public static LOADING_ICON:string = 'assets/preload/loading.png';

    public static ARROW_FALL:string = 'arrow-fall.png';
    public static ARROW_RISE:string = 'arrow-rise.png';
    public static BACKGROUND:string = 'background.jpg';
    public static CHARACTER_1_ARM:string = 'character-1-arm.png';
    public static CHARACTER_1:string = 'character-1.png';
    public static CHARACTER_2_ARM:string = 'character-2-arm.png';
    public static CHARACTER_2:string = 'character-2.png';
    public static CHARACTER_3_ARM:string = 'character-3-arm.png';
    public static CHARACTER_3:string = 'character-3.png';
    public static CHOOSE_CHARACTER_1:string = 'choose-character-1.png';
    public static CHOOSE_CHARACTER_2:string = 'choose-character-2.png';
    public static CHOOSE_CHARACTER_3:string = 'choose-character-3.png';
    public static COIN_ARROW_LEFT:string = 'coin-arrow-left.png';
    public static COIN_ARROW_RIGHT:string = 'coin-arrow-right.png';
    public static COIN_DURATION_20_YEARS:string = 'coin-duration-20-years.png';
    public static COIN_DURATION_5_YEARS:string = 'coin-duration-5-years.png';
    public static COIN:string = 'coin.png';

}