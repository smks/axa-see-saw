import Point = Phaser.Point;
export default class VerticalSliderModel {

    private currentCoords:Point;
    private minYPos:number;
    private maxYPos:number;

    /**
     *
     * @param yPos
     * @param minYPos
     * @param maxYPos
     */
    constructor(xPos:number, yPos:number, minYPos:number, maxYPos:number) {
        this.currentCoords = new Point(xPos, yPos);
        this.minYPos = minYPos;
        this.maxYPos = maxYPos;
        this.movePosition(yPos);
    }

    /**
     *
     * @param position
     */
    movePosition(yPosition:number) {

        if (yPosition < this.minYPos) {
            yPosition = this.minYPos;
        } else if (yPosition > this.maxYPos) {
            yPosition = this.maxYPos;
        }

        this.currentCoords.y = yPosition;
    }

    get xPos():number {
        return this.currentCoords.x;
    }

    get yPos():number {
        return this.currentCoords.y;
    }

    getPosition():Point {
        return this.currentCoords;
    }
}