import Point = Phaser.Point;
export default class HorizontalSliderModel {

    private currentCoords:Point;
    private minXPos:number;
    maxXPos:number;
    private currentNumberOfYears:number = 25;
    private maxNumberOfYears:number = 25;
    private increments:Object;
    isPulsing:boolean = true;

    /**
     *
     * @param yPos
     * @param minYPos
     * @param maxYPos
     */
    constructor(xPos:number, yPos:number, minXPos:number, maxXPos:number) {

        this.currentCoords = new Point(xPos, yPos);
        this.minXPos = minXPos;
        this.maxXPos = maxXPos;
        this.movePosition(xPos);

        var incrementStep = (this.maxXPos - this.minXPos) / this.maxNumberOfYears;
        this.increments = {};

        var currentIncrement = this.minXPos;
        for (var i = this.maxNumberOfYears; i >= 0; i--) {

            if (i === 0) {
                this.increments[i] = this.maxXPos;
                continue;
            }

            if (i === this.maxNumberOfYears) {
                this.increments[i] = this.minXPos;
                continue;
            }

            currentIncrement += incrementStep;
            this.increments[i] = currentIncrement;
        }
    }

    /**
     *
     * @param position
     */
    movePosition(xPosition:number) {

        if (xPosition < this.minXPos) {
            xPosition = this.minXPos;
        } else if (xPosition > this.maxXPos) {
            xPosition = this.maxXPos;
        }

        for (var key in this.increments) {
            if (this.increments.hasOwnProperty(key)) {
                if (xPosition <= this.increments[key]) {
                    this.currentNumberOfYears = parseInt(key);
                }
            }
        }

        this.currentCoords.x = xPosition;
    }

    get xPos():number {
        return this.currentCoords.x;
    }

    get yPos():number {
        return this.currentCoords.y;
    }

    get years():number {
        return this.currentNumberOfYears;
    }

    getPosition():Point {
        return this.currentCoords;
    }

    getMinX():number {
        return this.minXPos;
    }

    setPulse(isPulsing:boolean) {
        this.isPulsing = isPulsing;
    }
}