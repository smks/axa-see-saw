import Point = Phaser.Point;
export default class SeeSawModel {

    start:Point;
    end:Point;
    farRightX:number;

    /**
     *
     * @param position
     */
    update(startPoint:Point, endPoint:Point, farRightX:number) {
        this.start = startPoint;
        this.end = endPoint;
        this.farRightX = farRightX;
    }
}