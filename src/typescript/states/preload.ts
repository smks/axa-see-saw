import {IState} from "./../interfaces/istate";

export class Preloader extends Phaser.State implements IState {

    loading:Phaser.Sprite;

    preload() {

        this.game.stage.backgroundColor = "#ffffff";

        this.loading = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'loading');
        this.loading.anchor.set(0.5, 0.5);

        this.game.load.atlasJSONArray(
            'sprites',
            'assets/atlas/axa-see-saw.png',
            null,
            `{"frames": [
                {
                    "filename": "background.jpg",
                    "frame": {"x":2,"y":2,"w":740,"h":342},
                    "rotated": false,
                    "trimmed": false,
                    "spriteSourceSize": {"x":0,"y":0,"w":740,"h":342},
                    "sourceSize": {"w":740,"h":342}
                },
                {
                    "filename": "banker-arm.png",
                    "frame": {"x":69,"y":471,"w":78,"h":24},
                    "rotated": false,
                    "trimmed": false,
                    "spriteSourceSize": {"x":0,"y":0,"w":78,"h":24},
                    "sourceSize": {"w":78,"h":24}
                },
                {
                    "filename": "banker-head.png",
                    "frame": {"x":995,"y":2,"w":24,"h":30},
                    "rotated": false,
                    "trimmed": false,
                    "spriteSourceSize": {"x":0,"y":0,"w":24,"h":30},
                    "sourceSize": {"w":24,"h":30}
                },
                {
                    "filename": "banker.png",
                    "frame": {"x":2,"y":346,"w":65,"h":149},
                    "rotated": false,
                    "trimmed": false,
                    "spriteSourceSize": {"x":0,"y":0,"w":65,"h":149},
                    "sourceSize": {"w":65,"h":149}
                },
                {
                    "filename": "blue-sphere-light20.png",
                    "frame": {"x":223,"y":346,"w":46,"h":46},
                    "rotated": false,
                    "trimmed": false,
                    "spriteSourceSize": {"x":0,"y":0,"w":46,"h":46},
                    "sourceSize": {"w":46,"h":46}
                },
                {
                    "filename": "blue-sphere.png",
                    "frame": {"x":223,"y":394,"w":46,"h":46},
                    "rotated": false,
                    "trimmed": false,
                    "spriteSourceSize": {"x":0,"y":0,"w":46,"h":46},
                    "sourceSize": {"w":46,"h":46}
                },
                {
                    "filename": "pink-sphere-light20.png",
                    "frame": {"x":223,"y":442,"w":50,"h":43},
                    "rotated": false,
                    "trimmed": false,
                    "spriteSourceSize": {"x":0,"y":0,"w":50,"h":43},
                    "sourceSize": {"w":50,"h":43}
                },
                {
                    "filename": "pink-sphere.png",
                    "frame": {"x":271,"y":346,"w":50,"h":43},
                    "rotated": false,
                    "trimmed": false,
                    "spriteSourceSize": {"x":0,"y":0,"w":50,"h":43},
                    "sourceSize": {"w":50,"h":43}
                },
                {
                    "filename": "trees.png",
                    "frame": {"x":744,"y":2,"w":249,"h":36},
                    "rotated": false,
                    "trimmed": true,
                    "spriteSourceSize": {"x":1,"y":0,"w":249,"h":36},
                    "sourceSize": {"w":250,"h":36}
                },
                {
                    "filename": "year-duration.png",
                    "frame": {"x":69,"y":346,"w":152,"h":123},
                    "rotated": false,
                    "trimmed": false,
                    "spriteSourceSize": {"x":0,"y":0,"w":152,"h":123},
                    "sourceSize": {"w":152,"h":123}
                }],
                "meta": {
                    "app": "http://www.codeandweb.com/texturepacker",
                    "version": "1.0",
                    "image": "axa-see-saw.png",
                    "format": "RGBA8888",
                    "size": {"w":1024,"h":512},
                    "scale": "1",
                    "smartupdate": "$TexturePacker:SmartUpdate:2ba85951ffe3d9ccaf9059af3462883e:826e07fc41fc4e9054700b50972813c8:197cccb4b7690ee95b901ca79463e7f7$"
                }
                }`
        );
    }

    update() {
        this.loading.rotation += 0.1;
        if (this.loading.rotation >= 360) {
            this.loading.rotation = 0;
        }
    }

    create() {

        var tween = this.game.add.tween(this.loading)
            .to({alpha: 0}, 1000, "Linear", false);
        tween.onComplete.add(() => {
            this.game.state.start('Play', true);
        });
        tween.start();
    }

    shutdown() {
        this.loading = null;
    }
}