import {IState} from "../interfaces/istate";
import SeeSawView from "../views/seesaw";
import SeeSawController from "../controllers/seesaw.controller";
import SeeSawModel from "../models/seesaw";
import Banker from "../characters/banker";
import VerticalSliderModel from "../models/vertical-slider";
import VerticalSliderView from "../views/vertical-slider";
import VerticalSliderController from "../controllers/vertical-slider.controller";
import Point = Phaser.Point;
import HorizontalSliderModel from "../models/horizontal-slider";
import HorizontalSliderView from "../views/horizontal-slider";
import HorizontalSliderController from "../controllers/horizontal-slider.controller";

export class Play extends Phaser.State implements IState {

    private background;
    private seeSawController:SeeSawController;
    private verticalSliderController:VerticalSliderController;
    private horizontalSliderController:HorizontalSliderController;
    private banker:Banker;

    private characterChangeEvent:Phaser.Signal;
    private graphChangeUpEvent:Phaser.Signal;
    private graphChangeDownEvent:Phaser.Signal;

    private verticalSliderModel:VerticalSliderModel;
    private verticalSliderView:VerticalSliderView;
    private horizontalSliderModel:HorizontalSliderModel;
    private horizontalSliderView:HorizontalSliderView;
    private startPoint;
    private endPoint;
    private seeSawModel:SeeSawModel;
    private seeSawView:SeeSawView;
    private minEndY:number;
    private maxEndY:number;
    private midEndY:number;
    private trees:Phaser.Sprite;

    constructor() {
        super();
    }

    create() {

        this.graphChangeUpEvent = new Phaser.Signal();
        this.graphChangeDownEvent = new Phaser.Signal();

        this.background = this.game.add.sprite(0, 0, 'sprites');
        this.background.frameName = 'background.jpg';

        this.banker = new Banker(this.game);
        this.banker.x = 29;
        this.banker.y = 54;
        this.banker.create();

        this.seeSawModel = new SeeSawModel();
        this.seeSawView = new SeeSawView(this.game);
        this.seeSawController = new SeeSawController(this.seeSawModel, this.seeSawView);

        this.verticalSliderModel = new VerticalSliderModel(254, 169, 31, 194);
        this.verticalSliderView = new VerticalSliderView(this.game);
        this.verticalSliderController = new VerticalSliderController(this.verticalSliderModel, this.verticalSliderView);
        this.verticalSliderController.updateView();

        this.horizontalSliderModel = new HorizontalSliderModel(442, 167, 442, 652);
        this.horizontalSliderView = new HorizontalSliderView(this.game);
        this.horizontalSliderController = new HorizontalSliderController(this.horizontalSliderModel, this.horizontalSliderView);
        this.horizontalSliderController.updateView();

        this.trees = this.game.add.sprite(411, 306, 'sprites');
        this.trees.frameName = 'trees.png';

        this.startPoint = new Point(0, 0);
        this.endPoint = new Point(0, 0);

        this.minEndY = 135;
        this.maxEndY = 260;
        this.midEndY = (this.minEndY + this.minEndY / 2)

        this.setDefaultPosition();

    }
    
    update() {
        this.startPoint.x = this.verticalSliderModel.xPos;
        this.startPoint.y = this.verticalSliderModel.yPos;
        this.endPoint.x = this.horizontalSliderModel.xPos;
        this.endPoint.y = this.horizontalSliderModel.yPos;

        this.banker.move(this.verticalSliderModel.getPosition());

        this.horizontalSliderController.updateView();
        
        if (this.horizontalSliderController.shouldPulse && this.verticalSliderController.shouldPulse) {
            this.horizontalSliderController.updatePulse();
            this.verticalSliderController.updatePulse();
        }

        this.seeSawController.updateModel(
            this.startPoint,
            this.endPoint,
            this.horizontalSliderModel.maxXPos
        );
        this.seeSawController.updateView();
    }

    shutdown() {
        this.characterChangeEvent.dispose();
    }

    private setDefaultPosition() {
        this.horizontalSliderController.setPosition(566.8559670781893);
    }
}