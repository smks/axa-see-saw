import Group = Phaser.Group;
import addTextElement from "./../helpers/font-factory";
import Color = Phaser.Color;
import Tween = Phaser.Tween;
import Quadratic = Phaser.Easing.Quadratic;
import ColorMatrixFilter = PIXI.ColorMatrixFilter;
export default class HorizontalSliderView {

    game:Phaser.Game;
    private circle:Phaser.Sprite;
    private years:number = 25;
    private circleGroup:Group;
    private title:Phaser.Text;
    private label:Phaser.Sprite;

    /**
     *
     * @param game
     */
    constructor(game:Phaser.Game) {
        this.game = game;
    }

    create() {

        this.circleGroup = new Group(this.game);

        this.circle = this.game.add.sprite(0, 0, 'sprites', 'blue-sphere.png');
        this.circle.anchor.set(0.5, 0);
        this.circle.inputEnabled = true;
        this.circle.input.enableDrag(false, false, true, 1, new Phaser.Rectangle(419, 190, 257, 0));

        this.title = addTextElement(this.game, {
            xPos: this.circle.x,
            yPos: this.circle.y,
            content: this.years.toString(),
            fontSize: '30px',
            lineSpacing: 1,
            font: 'Myriad Pro',
            color: '#ffffff'
        });

        this.circleGroup.add(this.circle);
        this.circleGroup.add(this.title);

        this.label = this.game.add.sprite(0, 0, 'sprites', 'year-duration.png');
        this.label.anchor.set(0.5, 0);
    }

    /**
     *
     * @param dragStart
     * @param dragUpdate
     * @param dragStop
     */
    bindDragEvents(dragStart, dragUpdate, dragStop) {
        this.circle.events.onDragStart.add(dragStart);
        this.circle.events.onDragUpdate.add(dragUpdate);
        this.circle.events.onDragStop.add(dragStop);
    }

    /**
     *
     * @param xPos
     * @param yPos
     * @param angle
     */
    update(xPos:number, yPos:number, years:number) {
        this.circle.x = xPos;
        this.circle.y = yPos;
        this.title.x = this.circle.x;
        this.title.y = this.circle.centerY + 4;
        this.years = years;
        this.title.text = this.years.toString();

        this.label.x = this.circle.x - (this.label.width / 2) + 3;
        this.label.y = this.circle.y * 1.31;
    }

    xPosition():number {
        return this.circle.x;
    }

    yPosition():number {
        return this.circle.y;
    }

    pulse(shouldPulse:boolean = true) {

        if (!shouldPulse) {
            return;
        }

        this.circle.frameName = 'blue-sphere-light20.png';

        var tween = this.game.add
            .tween(this.circle.scale)
            .to({x: 1.1, y: 1.1}, 300, Quadratic.InOut, false, 1, 0, true);

        tween.onComplete.add(() => {
            this.circle.frameName = 'blue-sphere.png';
        });

        tween.start();
    }
}