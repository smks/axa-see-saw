import Quadratic = Phaser.Easing.Quadratic;
export default class VerticalSliderView {

    game:Phaser.Game;
    private circle:Phaser.Sprite;

    /**
     *
     * @param game
     */
    constructor(game:Phaser.Game) {
        this.game = game;
    }

    create() {
        this.circle = this.game.add.sprite(0, 0, 'sprites', 'pink-sphere.png');
        this.circle.anchor.set(0.5, 0.5);
        this.circle.inputEnabled = true;
        this.circle.input.enableDrag(false, false, true, 1, new Phaser.Rectangle(226, 10, 0, 209));
    }

    /**
     *
     * @param dragStart
     * @param dragUpdate
     * @param dragStop
     */
    bindDragEvents(dragStart, dragUpdate, dragStop) {
        this.circle.events.onDragStart.add(dragStart);
        this.circle.events.onDragUpdate.add(dragUpdate);
        this.circle.events.onDragStop.add(dragStop);
    }

    /**
     *
     * @param xPos
     * @param yPos
     * @param angle
     */
    update(xPos:number, yPos:number) {
        this.circle.x = xPos;
        this.circle.y = yPos;
    }

    pulse(shouldPulse:boolean = true) {

        if (!shouldPulse) {
            return;
        }

        this.circle.frameName = 'pink-sphere-light20.png';

        var tween = this.game.add
            .tween(this.circle.scale)
            .to( { x: 1.1, y: 1.1 }, 300, Quadratic.InOut, false, 1, 0, true);

        tween.onComplete.add(() => {
            this.circle.frameName = 'pink-sphere.png';
        });

        tween.start();
    }
}