import Point = Phaser.Point;
import BitmapData = Phaser.BitmapData;
export default class SeeSawView {

    game:Phaser.Game;
    private bmd:BitmapData;
    private sprite:Phaser.Sprite;

    /**
     *
     * @param game
     */
    constructor(game:Phaser.Game) {
        this.game = game;
    }

    create() {
        this.bmd = this.game.add.bitmapData(this.game.width, this.game.height);
        this.bmd.ctx.beginPath();
        this.bmd.ctx.lineWidth = 5;
        this.bmd.ctx.strokeStyle = 'rgb(226, 0, 122)';
        this.bmd.ctx.stroke();
        this.sprite = this.game.add.sprite(0, 0, this.bmd);
    }

    /**
     *
     * @param xPos
     * @param yPos
     * @param angle
     */
    update(start:Point, mid:Point, farRightX:number = 0) {

        var point1 = new Point(start.x, start.y); // P1
        var point2 = new Point(mid.x, mid.y); // P2
        var point3 = new Point(farRightX, point1.y); // P3

        var angle:number = Math.atan2(point2.y - point1.y, point2.x - point1.x);
        var height:number = Math.tan(angle) * (point3.getMagnitude() - point1.getMagnitude());
        var endPoint = new Point(point3.x, point3.y + height);

        this.bmd.clear();
        this.bmd.ctx.beginPath();
        this.bmd.ctx.moveTo(start.x, start.y);
        this.bmd.ctx.lineTo(endPoint.x, endPoint.y);
        this.bmd.ctx.lineWidth = 4;
        this.bmd.ctx.stroke();
        this.bmd.ctx.closePath();
        this.bmd.render();
    }
}