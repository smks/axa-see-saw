/// <reference path='../../node_modules/phaser/typescript/phaser' />

import Game = Phaser.Game;

import {Boot} from "./states/boot";
import {Preloader} from "./states/preload";
import {Play} from "./states/play";

class MyGame {
    game:Game;

    constructor() {

        this.game = new Game(
            740,
            342,
            Phaser.AUTO,
            'game', {
                create: this.create
            }
        );
    }

    create() {

        this.game.state.add('Boot', Boot, false);
        this.game.state.add('Preloader', Preloader, false);
        this.game.state.add('Play', Play, false);

        this.game.state.start('Boot');
    }

    public destroy() {

    }
}


window.onload = () => {
    window['seesaw'] = new MyGame();
};