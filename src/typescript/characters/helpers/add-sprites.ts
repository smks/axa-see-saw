import Sprite = Phaser.Sprite;
import Coords from "./coords";

/**
 *
 * @param characterId
 * @param game
 * @returns {Phaser.Group}
 */
export function getCharacter(game:Phaser.Game):Sprite {
    var characterSprite = game.add.sprite(0, 0, 'sprites')
    characterSprite.frameName = `banker.png`;
    return characterSprite;
}

/**
 *
 * @param characterId
 * @param game
 * @returns {Phaser.Group}
 */
export function getCharacterArm(game:Phaser.Game):Sprite {
    var characterArm = game.add.sprite(0, 0, 'sprites')
    characterArm.x = 36;
    characterArm.y = 24;
    characterArm.angle = -45;
    characterArm.anchor.set(0, 0.5);
    characterArm.frameName = `banker-arm.png`;
    return characterArm;
}

/**
 *
 * @param characterId
 * @param game
 * @returns {Phaser.Group}
 */
export function getCharacterHead(game:Phaser.Game):Sprite {
    var characterHead = game.add.sprite(0, 0, 'sprites');
    characterHead.x = 49;
    characterHead.y = 0;
    characterHead.anchor.set(0.5, 0.5);
    characterHead.frameName = `banker-head.png`;
    return characterHead;
}