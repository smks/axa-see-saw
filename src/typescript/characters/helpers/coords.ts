export default class Coords {
    private _xPos:number;
    private _yPos:number;
    private _angle:number = 0;
    private _width:number = 0;

    /**
     *
     * @param parameters
     */
    constructor(parameters) {
        this._xPos = parameters.xPos;
        this._yPos = parameters.yPos;
        this._angle = parameters.angle;
        this._width = parameters.width;
    }

    get xPos():number {
        return this._xPos;
    }

    get yPos():number {
        return this._yPos;
    }

    get angle():number {
        return this._angle;
    }

    set xPos(xpos) {
        this._xPos = xpos;
    }

    set yPos(ypos) {
        this._yPos = ypos;
    }

    set angle(angle) {
        this._angle = angle;
    }

    set width(width) {
        this._width = width;
    }
}