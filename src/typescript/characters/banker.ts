import {getCharacter, getCharacterArm, getCharacterHead} from "../characters/helpers/add-sprites";
import Point = PIXI.Point;

export default class Banker extends Phaser.Group  {

    game:Phaser.Game;
    characterSprite:Phaser.Sprite;
    characterArm:Phaser.Sprite;
    characterHead:Phaser.Sprite;

    /**
     *
     * @param character
     * @param game
     */
    constructor(game:Phaser.Game) {
        super(game);
        this.game = game;
    }

    create() {
        this.characterSprite = getCharacter(this.game);
        this.characterHead = getCharacterHead(this.game);
        this.characterArm = getCharacterArm(this.game);
        this.add(this.characterArm);
        this.add(this.characterSprite);
        this.add(this.characterHead);
    }

    /**
     *
     * @param position
     */
    move(verticalSliderPosition:Point) {

        var angleBetweenHead = Phaser.Math.angleBetween(
            this.characterHead.x,
            this.characterHead.y,
            verticalSliderPosition.x,
            verticalSliderPosition.y
        );

        var angleBetweenArm = Phaser.Math.angleBetween(
            this.characterArm.x,
            this.characterArm.y,
            verticalSliderPosition.x,
            verticalSliderPosition.y
        );

        this.characterArm.angle = angleBetweenArm * 90;
        this.characterHead.angle = angleBetweenHead * 50;
    }
}