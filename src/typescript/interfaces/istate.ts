export interface IState {
    create();
    update();
    shutdown();
}