export interface ITranslate {
    translate(key:string):string;
}