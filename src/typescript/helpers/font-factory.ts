export default function (game, options, underline:boolean = false) {
    var text = game.add.text(
        options.xPos, options.yPos, options.content, {
            font: options.fontSize + ' ' + options.font,
            fill: options.color,
            align: "center"
        });
    text.anchor.set(0.5, 0.5);
    text.lineSpacing = options.lineSpacing;
    if (underline) {
        text.fontStyle = 'underline';
    }
    return text;
}